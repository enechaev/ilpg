#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include "indlauxil.h"

#include "indnode.h"

int addrule(struct state *s,const char *const t,const char *const f,struct node *expr) {
	if (!s->rcount || !s->rules) s->rules=(struct rule *)malloc((s->rcount=1)*sizeof(struct rule));
	else s->rules=(struct rule *)realloc(s->rules,++s->rcount*sizeof(struct rule));
	if (!s->rules) return -1;
	s->rules[s->rcount-1].expr=expr;
	int i;
	s->rules[s->rcount-1].nterm=(i=findterm(&s->nonterms,t))==-1?insertterm(&s->nonterms,t):i;
	s->rules[s->rcount-1].flag=f?findterm(&s->flags,f):-1;

}

struct symlist ilps_init() {
	struct symlist s;
	s.count=0;
	s.termlist=NULL;
	return s;
}

struct state state_init() {
	struct state s;
	s.terms=ilps_init();
	s.flags=ilps_init();
	s.nonterms=ilps_init();
	s.rcount=0;
	s.rules=NULL;
	return s;
}

int ilps_destroy(struct symlist s) {
	if (s.termlist) {
		for (unsigned int i=0;i<s.count;i++) if (s.termlist[i]) free(s.termlist[i]);
		free(s.termlist);
	}
	return 0;
}

int state_destroy(struct state s) {
	ilps_destroy(s.terms);
	ilps_destroy(s.flags);
	ilps_destroy(s.nonterms);
	if (s.rules) {
		for (unsigned int i=0;i<s.rcount;i++) destroynode(s.rules[i].expr);
		free(s.rules);
	}
	return 0;
}

int insertterm(struct symlist *s,const char *const c) {
	if (!s->count || !s->termlist) s->termlist=(char **)malloc((s->count=1)*sizeof(char *));
	else s->termlist=(char **)realloc(s->termlist,++s->count*sizeof(char *));
	if (!s->termlist) return -1;
	s->termlist[s->count-1]=(char *)malloc((strlen(c)+1)*sizeof(char));
	if (!s->termlist[s->count-1]) return -1;
	strcpy(s->termlist[s->count-1],c);
	return s->count-1;
}

int findterm(const struct symlist *const s,const char *const t) {
	if (!s->count) return -1;
	for(unsigned int i=0;i<s->count;i++) if (!strcmp(s->termlist[i],t)) return i;
	return -1;
}

