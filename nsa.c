#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

#include <stdio.h>

typedef int T;

struct list {
	struct list *up;
	struct list *down;
	bool istop;
	enum nodetype {VALUE,BOTTOM} type;
	T value;
};

struct list *push(struct list *current,T c) {
	if (!current || !current->istop) return NULL;
	struct list *l=(struct list *)malloc(sizeof(struct list));
	if (!l) return NULL;
	l->up=NULL;
	l->down=current;
	l->type=VALUE;
	l->value=c;
	l->istop=true;
	current->istop=false;
	current->up=l;
	return l;
}

struct list *pop(struct list *current,T *retval) {
	if (!current || !current->istop || current->type==BOTTOM) return NULL;
	struct list *l=current->down;
	l->up=NULL;
	l->istop=true;
	*retval=current->value;
	free(current);
	return l; 
}

struct list *movedown(struct list *current) {
	if (!current || !current->down) return NULL;
	return current->down;
}

struct list *moveup(struct list *current) {
	if (!current || current->istop) return NULL;
	return current->up;
}

struct list *mkstack(struct list *current,unsigned int length, ...) {
	struct list *l=(struct list *)malloc(sizeof(struct list));
	if (!l) return NULL;
	l->up=current->up;
	l->down=current->down;
	l->type=BOTTOM;
	l->istop=true;
	current->istop=false;
	
	va_list ap;
	va_start(ap,length);
	T p;
	for (unsigned int i=0;i<length;i++,p=va_arg(ap,T)) l=push(l,p);
	va_end(ap);
	return l;
}

struct list *initstack() {
	struct list *l=(struct list *)malloc(sizeof(struct list));
	if (!l) return NULL;
	l->up=NULL;
	l->down=NULL;
	l->type=BOTTOM;
	l->istop=true;
	return l;
}

int delstack(struct list *current) {
	if (!current) return 1;
	if(current->up) {
		delstack(current->up);
		//free(current->up);
		current->up=NULL;
	}
	if(current->down) {
		delstack(current->down);
		//free(current->down);
		current->down=NULL;
	}
	free(current);
	return 0;
}

int main() {
	struct list *l=initstack();
	l=push(l,3);
	l=push(l,2);
	l=movedown(l);
	l=moveup(l);
	int p;
	l=pop(l,&p);
	printf("1:%d\n",p);
	l=pop(l,&p);
	printf("2:%d\n",p);
	delstack(l);	
	return 0;
}
