#ifndef __ILP_AUXIL_
#define __ILP_AUXIL_ 1

struct symlist {
	char **termlist;
	int count;
};

struct rule {
	unsigned int nterm;
	unsigned int flag;
	struct node *expr;
};

struct state {
	struct symlist terms,nonterms,flags;
	struct rule *rules;
	unsigned int rcount;
};

extern int addrule(struct state *,const char *const,const char *const,struct node *);
extern struct symlist ilps_init();
extern int ilps_destroy(struct symlist);
extern struct state state_init();
extern int state_destroy(struct state);
extern int insertterm(struct symlist *,const char *const);
extern int findterm(const struct symlist *const,const char *const);

/*struct pstate {
	struct symlist terms;
	struct symlist nonterms;
	struct symlist flags;
};*/

#endif
