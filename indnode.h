#ifndef __INDNODE__
#define __INDNODE__ 1

enum nodetype {
	NSEQ,NFLG,NALT,LEAF,STR
};

enum symtype {
	TERM,NONTERM,FLAG
};

struct node;

struct symbol {
	enum symtype type;
	unsigned int ref;
	
};

struct flagged {
	struct node *nonterm;
	struct node *flags;
};

struct sequence {
	struct node *left;
	struct node *right;
};

struct alternative {
	struct node *left;
	struct node *right;
};

struct string {
	char *s;
};

union nodedata {
	struct alternative alt;
	struct sequence    seq;
	struct symbol      sym;
	struct flagged     flg;
	struct string      str;
};

struct node {
	enum  nodetype type;
	union nodedata data;
};

struct node *mkseq(struct state *,struct node *,struct node *);
struct node *mkalt(struct state *,struct node *,struct node *);
struct node *mkflg(struct state *,struct node *,struct node *);
struct node *mksymbol(struct state *,enum symtype,const char *const);
struct node *mkstring(struct state *,const char *const);
int destroynode(struct node *);

/*struct node *mknode(enum nodetype t) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	switch(t) {
		case LEAF:
			n->type=LEAF;
			break;
		case NALT:
			n->type=NALT;
			break;
		case NSEQ:
			n->type=NSEQ;
			break;
		default: return NULL;
	}
}*/

#endif
