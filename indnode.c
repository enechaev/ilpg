#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "indlauxil.h"
#include "indnode.h"

/*
struct node *mk (struct state *s,) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	
	return n;
}
*/

struct node *mkflg(struct state *s,struct node *nt,struct node *f) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	n->type=NFLG;
	n->data.flg.nonterm=nt;
	n->data.flg.flags=f;
	return n;
}

struct node *mkalt(struct state *s,struct node *l,struct node *r) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	n->type=NALT;
	n->data.seq.left=l;
	n->data.seq.right=r;
	return n;
}

struct node *mkseq(struct state *s,struct node *l,struct node *r) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	n->type=NSEQ;
	n->data.seq.left=l;
	n->data.seq.right=r;
	return n;
}

struct node *mksymbol(struct state *s,enum symtype T,const char *const t) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	n->type=LEAF;
	n->data.sym.type=T;
	switch(T) {
		case TERM:
			n->data.sym.ref=findterm(&s->terms,t);
			if (n->data.sym.ref==-1) {free(n);return NULL;}
			break;
		case FLAG:
			n->data.sym.ref=findterm(&s->flags,t);
			if (n->data.sym.ref==-1) {free(n);return NULL;}
			break;
		default: {
			if (findterm(&s->flags,t)!=-1 || findterm(&s->terms,t)!=-1) return NULL;
			int i=-1;
			n->data.sym.ref=((i=findterm(&s->nonterms,t))!=-1)?i:insertterm(&s->nonterms,t);
		}
	}
	return n;
};

struct node *mkstring(struct state *s,const char *const t) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	n->type=STR;
	n->data.str.s=(char *)malloc(sizeof(char)*(strlen(t)+1));
	if (!n->data.str.s) {free(n); return NULL;}
	strcpy(n->data.str.s,t);
	return n;
}

int destroynode(struct node *n) {
	if (!n) return -1;
	switch(n->type) {
		case STR:
			free(n->data.str.s); break;
		case NALT:
			destroynode(n->data.alt.left);
			destroynode(n->data.alt.right);
			break;
		case NSEQ:
			destroynode(n->data.seq.left);
			destroynode(n->data.seq.right);
			break;
		case LEAF: break;
		case NFLG:
			destroynode(n->data.flg.nonterm);
			destroynode(n->data.flg.flags);
			break;
	}
	free(n);
	return 0;
}

/*struct node *mknode(enum nodetype t) {
	struct node *n=(struct node *)malloc(sizeof(struct node));
	if (!n) return NULL;
	switch(t) {
		case LEAF:
			n->type=LEAF;
			break;
		case NALT:
			n->type=NALT;
			break;
		case NSEQ:
			n->type=NSEQ;
			break;
		default: return NULL;
	}
}*/
